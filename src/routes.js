import React from 'react';
import {IndexRoute, Route} from 'react-router';
import { isLoaded as isAuthLoaded, load as loadAuth } from './redux/modules/auth';

import {
  App,
  Home,
  User,
  DevApp,
  Login,
  NotFound
} from 'containers';

import * as sgUserActions from "./redux/sagas/sgUsers";
import * as sgLoginActions from "./redux/sagas/sgLogin";

// function callback(a, b, c, d, e) {
//
//   return;
// }

function loadUsers({dispatch, getState}, nextState, replaceState, callback) {
  const id = "ZUMZUM";
  if (!getState().users.userRepos) {
    dispatch({type: sgUserActions.REPOS_FETCH_REQUESTED, payload: id, callback: callback});
  } else {
    return callback();
  }
}

function resetUsers({dispatch, getState}, nextState, replaceState, callback) {
  dispatch({type: sgUserActions.REPOS_RESET_REQUESTED});
}

class Container extends React.Component {
  render() {
    return <div className="container-component">{this.props.children}</div>
  }
}

export default (store) => {
  const __loadUsers = (nextState, replaceState, callback) => loadUsers(store, nextState, replaceState, callback);
  const __resetUsers = (nextState, replaceState, callback) => resetUsers(store, nextState, replaceState, callback);

  const requireLogin = (nextState, replace, cb) => {
    const __cb = () => {
      const state = store.getState();
      // if(!state.auth.user.user) {
        //replace('/login');
      // }
      cb();
    };
    
    if (true ||!store.getState().auth.user) {
      store.dispatch({type: sgLoginActions.LOAD_REQUESTED, payload: null, callback: __cb});
    } else {
      cb();
    }

  };


  return (
    /**
     * Please keep routes in alphabetical order
     */
    <Route component={Container}>
      <Route path="/" component={App} onEnter={requireLogin}>
        { /* Home (main) route */ }
        <IndexRoute component={Home}/>

        <Route path="/user" component={User} onEnter={__loadUsers} onLeave={__resetUsers}/>
        <Route path="/app" component={DevApp}/>
      </Route>

      <Route path="/login" component={Login}/>

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404}/>
    </Route>
  );
}
