import React, {Component, PropTypes} from 'react';
import { connect } from "react-redux";
import {IndexLink} from 'react-router';
import {LinkContainer} from 'react-router-bootstrap';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Helmet from 'react-helmet';
import { UserProfileBadge } from 'components';
import config from '../../config';



export default class App extends Component {
  render() {
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>

        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                      aria-expanded="false" aria-controls="navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="brand navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
              <ul className="nav navbar-nav navbar-right">
                <li className={styles.userProfile}><UserProfileBadge/></li>
                <LinkContainer to="/"><NavItem eventKey="{0}">Dashboard</NavItem></LinkContainer>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Help</a></li>
              </ul>
            </div>
          </div>
        </nav>


        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-3 col-md-2 sidebar">
              <ul className="nav nav-sidebar">
                <LinkContainer to="/app"><NavItem eventKey={1}>Apps</NavItem></LinkContainer>
                <LinkContainer to="/user"><NavItem eventKey={2}>Users</NavItem></LinkContainer>
                <li><a href="#">Domains</a></li>
                <li><a href="#">Scopes</a></li>
              </ul>
            </div>

            <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


              <div className={styles.appContent}>
                {this.props.children}
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
