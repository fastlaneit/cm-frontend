import React, {Component, PropTypes}  from "react";
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import * as sgUserActions from "redux/sagas/sgUsers"

class User extends Component {
  render() {
    const styles = require('./User.scss') || {};
    // require the logo image both from client and server
    const { userRepos, refreshClicked } = this.props;

    console.log("rendering user repos: " + JSON.stringify(userRepos));

    const Repos = (userRepos ||  []).map(
      (item)=> (<div key={item.name} className="list-group-item">
        { item.name }
      </div>));

    return (
      <div className={styles.home}>
        <h1>User</h1>
        { Repos }
        <br/>
        <Button bsStyle="primary" onClick={refreshClicked}>Primary</Button>
      </div>
    );
  }
}

User.propTypes = {
  refreshClicked: PropTypes.func.isRequired,
  userRepos: PropTypes.array.isRequired
};



const select = (state) => {
  return {userRepos: state.users.userRepos };
};

const _dispatch = (dispatch) => {
  return {
    refreshClicked: (event) => {
      dispatch({type: sgUserActions.REPOS_FETCH_REQUESTED, payload: event.screenX});
    }
  }
};

export default connect(select, _dispatch)(User);
