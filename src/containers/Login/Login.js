import React, {Component} from "react";
import {Grid, Row, Col, Button, Form, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';

export default class Login extends Component {
  render() {
    const styles = require('./Login.scss') || {};
    // require the logo image both from client and server
    return (
      <div className={styles.loginForm}>
        <Form horizontal>
          <Row>
            <Col xs={12}><h1>Login</h1></Col>
          </Row>
          <FormGroup controlId="formHorizontalEmail">
            <Col componentClass={ControlLabel} sm={2}>
              Email
            </Col>
            <Col sm={10}>
              <FormControl type="email" placeholder="Email"/>
            </Col>
          </FormGroup>
          <FormGroup controlId="formHorizontalEmail">
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={10}>
              <FormControl type="password"/>
            </Col>
          </FormGroup>
          <Button bsStyle="primary">Login</Button>
        </Form>
      </div>
    );
  }
}
