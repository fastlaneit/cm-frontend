import restful, {fetchBackend} from 'restful.js';
import polyfill from 'es6-promise'
import fetch from 'isomorphic-fetch';

polyfill.polyfill();
// This is supposed to be the real API call

const __api = restful('http://localhost:8080/v1', fetchBackend(fetch));

const __loadUserRepos = (id) => {
  return __api
    .addRequestInterceptor ( ({url}) => console.log("REQUESTING URL:" + url))
    .all("user").getAll({amount: 25}).then((response) => {
    const repos = response.body();
    return (repos || []).map((repo) => repo.data());
  })
};

export default __loadUserRepos;
