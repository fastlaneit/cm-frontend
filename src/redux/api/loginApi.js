import restful, {fetchBackend} from 'restful.js';
import polyfill from 'es6-promise'
import fetch from 'isomorphic-fetch';

polyfill.polyfill();
// This is supposed to be the real API call

const __api = restful('http://localhost:8080/v1/sso', fetchBackend(fetch));
//__api.addRequestInterceptor(({url}) => console.log("REQUESTING URL:" + url))
__api.addRequestInterceptor((config) => {
  const { data, headers, method, params, url } = config;
  headers['Access-Control-Allow-Credentials'] = true;
  config.credentials = 'include';
  return config;
});

const __loginApi = (id) => {
  return __api
    .get({}).then((response) => {
      const repo = response.body();
      return (repo.data || (() => {}))();
    })
};

export default __loginApi;
