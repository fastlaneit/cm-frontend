import createSagaMiddleware from 'redux-saga'
import { take, put, call, fork, select } from 'redux-saga/effects'
import watchUsers from './sgUsers';
import watchAuth from './sgLogin';

export const sagaMiddleware = createSagaMiddleware();

export function runSaga(saga) {
  return sagaMiddleware.run(saga);
}


export default function* watch() {
  yield [
    fork(watchAuth),
    fork(watchUsers)
  ]
};