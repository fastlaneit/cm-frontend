import {takeEvery, takeLatest} from 'redux-saga'
import {call, put} from 'redux-saga/effects'

import loginApi from "../api/loginApi"

export const LOAD_REQUESTED = 'AUTH_LOAD_REQUESTED';
export const LOAD_SUCCESS = 'redux-example/auth/LOAD_SUCCESS';
export const LOAD_FAIL = 'redux-example/auth/LOAD_FAIL';

export const LOGIN = 'redux-example/auth/LOGIN';
export const LOGIN_SUCCESS = 'redux-example/auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = 'redux-example/auth/LOGIN_FAIL';
export const LOGOUT = 'redux-example/auth/LOGOUT';
export const LOGOUT_SUCCESS = 'redux-example/auth/LOGOUT_SUCCESS';
export const LOGOUT_FAIL = 'redux-example/auth/LOGOUT_FAIL';


function *loadAuth(action) {
  try {
    const repo = yield call(loginApi, action.payload);
    yield put({type: LOAD_SUCCESS, payload: repo});
  } catch (e) {
    yield put({type: LOAD_FAIL, message: e.message});
  }

  if(action.callback) {
    yield call(action.callback);
  }
}

function* watchAuth() {
  yield* takeLatest(LOAD_REQUESTED, loadAuth);
}

export default watchAuth;