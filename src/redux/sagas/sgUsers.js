import {takeEvery, takeLatest} from 'redux-saga'
import {call, put} from 'redux-saga/effects'

import userApi from "../api/userApi"

export const REPOS_FETCH_REQUESTED = 'REPOS_FETCH_REQUESTED';
export const REPOS_FETCH_SUCCEEDED = 'REPOS_FETCH_SUCCEEDED';
export const REPOS_FETCH_FAILED = 'REPOS_FETCH_FAILED';
export const REPOS_RESET_REQUESTED = 'REPOS_RESET_REQUESTED';


function *loadUserRepos(action) {
  try {
    const repos = yield call(userApi, action.payload);
    yield put({type: REPOS_FETCH_SUCCEEDED, payload: repos});
  } catch (e) {
    yield put({type: REPOS_FETCH_FAILED, message: e.message});
  }

  if(action.callback) {
    yield call(action.callback);
  }
}

function* watchUsers() {
  yield* takeLatest(REPOS_FETCH_REQUESTED, loadUserRepos);
}

export default watchUsers;