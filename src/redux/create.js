import {routerReducer, routerMiddleware} from "react-router-redux";
import {applyMiddleware, createStore as _createStore, combineReducers, compose} from "redux";
import reducer from "./modules/reducer"
import sagas from "./sagas/saga"
import "isomorphic-fetch";
import {sagaMiddleware} from "./sagas/saga"

export default function createStore(history, client, data) {


  const reduxRouterMiddleware = routerMiddleware(history);
  const middleware = [sagaMiddleware, reduxRouterMiddleware];

  let finalCreateStore;
  if (__DEVELOPMENT__ && __CLIENT__ && __DEVTOOLS__) {
    const { persistState } = require('redux-devtools');
    const DevTools = require('../containers/DevTools/DevTools');
    finalCreateStore = compose(
      applyMiddleware(...middleware),
      window.devToolsExtension ? window.devToolsExtension() : DevTools.instrument(),
      persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
    )(_createStore);
  } else {
    finalCreateStore = applyMiddleware(...middleware)(_createStore);
  }

  const store = finalCreateStore(reducer, data);
  sagaMiddleware.run(sagas);


  return store;
}
