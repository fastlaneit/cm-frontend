import * as sgLoginSagas from "../sagas/sgLogin"

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case sgLoginSagas.LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.payload
      };
    case sgLoginSagas.LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded;
}

