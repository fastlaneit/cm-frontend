import * as sgUserEvents from "../sagas/sgUsers"


function usersReducer(state = {}, action) {
  switch (action.type) {
    case sgUserEvents.REPOS_FETCH_REQUESTED:
      return Object.assign({}, state, {
        reposLoading: true
      });
    case sgUserEvents.REPOS_FETCH_SUCCEEDED:
      console.log("RUNNING REPOS_FETCH_SUCCEEDED...");
      return Object.assign({}, state, {
        userRepos: action.payload
      });
    case sgUserEvents.REPOS_FETCH_FAILED:
      console.log("REPOS_FETCH_FAILED: " +  action.message);
    case sgUserEvents.REPOS_RESET_REQUESTED:
      return Object.assign({}, state, {
        userRepos: null
      });
    default:
      return state
  }
}

export default usersReducer;