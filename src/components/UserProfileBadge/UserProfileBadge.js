import React, {Component, PropTypes} from 'react';
import { connect } from "react-redux";

@connect(
  state => ({user: state.auth.user})
)
export default class UserProfileBadge extends Component {
  static propTypes = {
    user: PropTypes.object
  };

  render() {
    const {token, user} = (this.props.user || {});
    return (
      <div>{user}</div>
    );
  }
}

